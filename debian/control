Source: keychain
Section: net
Priority: optional
Maintainer: Peter Pentchev <roam@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-single-binary,
Standards-Version: 4.6.0
Homepage: https://www.funtoo.org/Keychain
Vcs-Git: https://salsa.debian.org/debian/keychain.git
Vcs-Browser: https://salsa.debian.org/debian/keychain
Rules-Requires-Root: no

Package: keychain
Architecture: all
Multi-Arch: foreign
Depends: openssh-client | ssh-client, ${misc:Depends}
Suggests: gnupg-agent, ssh-askpass
Description: key manager for OpenSSH
 Keychain is an OpenSSH key manager, typically run from ~/.bash_profile. When
 keychain is run, it checks for a running ssh-agent, otherwise it starts one.
 It saves the ssh-agent environment variables to ~/.keychain/\$\{HOSTNAME\}-sh,
 so that subsequent logins and non-interactive shells such as cron jobs can
 source the file and make passwordless ssh connections.  In addition, when
 keychain runs, it verifies that the key files specified on the command-line
 are known to ssh-agent, otherwise it loads them, prompting you for a password
 if necessary.
